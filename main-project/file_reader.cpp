#include "file_reader.h"
#include "constants.h"

#include <fstream>
#include <cstring>


int dok_time(protokol_subscription* element)
{
    int hours, minute,second;
    hours = element->finish.hour - element->start.hour;
    minute = element->finish.min - element->start.min;
    second = element->finish.second - element->start.second;
    while (hours != 0)
    {
        minute = minute + 60;
        hours--;
    }
    while (minute != 0)
    {
        second = second + 60;
        minute--;
    }
    return second;
}

date convert(char* str)
{
    date result;
    char* context = NULL;
    char* str_number = strtok_s(str, ".", &context);
    result.hour = atoi(str_number);
    str_number = strtok_s(NULL, ".", &context);
    result.min = atoi(str_number);
    str_number = strtok_s(NULL, ".", &context);
    result.second = atoi(str_number);

    return result;
}


void read(const char* file_name, protokol_subscription* array[], int& size)
{
    std::ifstream file(file_name);
    if (file.is_open())
    {
        size = 0;
        char tmp_buffer[MAX_STRING_SIZE];
        while (!file.eof())
        {
            protokol_subscription* item = new protokol_subscription;
            file >> tmp_buffer;
            item->start = convert(tmp_buffer);
            file >> tmp_buffer;
            item->finish = convert(tmp_buffer);
            file >> item->receive.bite;
            file >> item->send.bite;
        
            file.read(tmp_buffer, 1); // ������ ������� ������� �������
            file.getline(item->mess, MAX_STRING_SIZE);
            item->length = dok_time(item);
            item->sum = item->receive.bite + item->send.bite;
            array[size++] = item;
        }
        file.close();
    }
    else
    {
        throw "������ �������� �����";
    }
}