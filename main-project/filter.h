#ifndef FILTER_H
#define FILTER_H
#include "protokol_subscription.h"
protokol_subscription** filter(protokol_subscription* array[], int size, bool (*check)(protokol_subscription* element), int& result_size);
bool check_by_mess(protokol_subscription* element);
bool check_by_time(protokol_subscription* element);
void siftDown(protokol_subscription** report, int root, int bottom);
void InsertionSort(protokol_subscription** report, int size);
void Quick(protokol_subscription** report, int size);
void siftDown_str(protokol_subscription** report, int root, int bottom);
void InsertionSort_str(protokol_subscription** report, int size);
void Quick_str(protokol_subscription** report, int size);
void mess_sort(protokol_subscription** report, int size);
#endif