#include <iostream>
#include "protokol_subscription.h"
#include "file_reader.h"
#include "constants.h"
#include "filter.h"
using namespace std;

void output(protokol_subscription* subscriptions)
{ 
    // ����� ������� ������
    if (subscriptions->start.hour < 61)
        cout << subscriptions->start.hour << ' ';
    else
        cout << subscriptions->start.hour << ' ';
    if (subscriptions->start.min < 61)
        cout << subscriptions->start.min << ' ';
    else
        cout << subscriptions->start.min << ' ';
    if (subscriptions->start.second < 61)
        cout << subscriptions->start.second << '\n';
    else
        cout << subscriptions->start.second << '\n';
    //����� ������� ���������
    if (subscriptions->finish.hour < 61)
        cout << subscriptions->finish.hour << ' ';
    else
        cout << subscriptions->finish.hour << ' ';
    if (subscriptions->finish.min < 61)
        cout << subscriptions->finish.min << ' ';
    else
        cout << subscriptions->finish.min << ' ';
    if (subscriptions->finish.second < 61)
        cout << subscriptions->finish.second << '\n';
    else
        cout << subscriptions->finish.second << '\n';



    cout << subscriptions->receive.bite << endl;
    cout << subscriptions->send.bite << '\n';
    cout << subscriptions->mess << '\n';
    cout << '\n';
}

int main()
{
    setlocale(LC_ALL, "ru");
    cout << "Laboratory work #9. GIT\n";
    cout << "Variant #5. Internet Work Protokol\n";
    cout << "Author: Evgeniy Gospodarik\n";
    cout << "Group: 12\n";
    protokol_subscription* subscriptions[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data.txt", subscriptions, size);
        for (int i = 0; i < size; i++)
        {
            output(subscriptions[i]);
        }
        bool (*check_function)(protokol_subscription*) = NULL;
        cout << "\n�������� ������ ���������� ��� ��������� ������:\n";
        cout << "1) ������������� Skype\n";
        cout << "2) ������������� ��������� ����� 8:00:00\n";
        cout << "\n������� ����� ���������� ������: ";
        int item;
        cin >> item;
        cout << '\n';
        switch (item)
        {
        case 1:
            check_function = check_by_mess; // ����������� � ��������� �� ������� ��������������� �������
            cout << "***** ������������� Skype *****\n\n";
            break;
        case 2:
            check_function = check_by_time; // ����������� � ��������� �� ������� ��������������� �������
            cout << "***** ������������� ��������� ����� 8:00:00 *****\n\n";
            break;
        default:
            throw "������������ ����� ������";
        }
        if (check_function)
        {
            int new_size;
            protokol_subscription** filtered = filter(subscriptions, size, check_function, new_size);
            cout << "�������� ��������:\n";
            cout << "1)���������� ��������� �� �������� ������������� ���������\n";
            cout << "2)������� ���������� �� �������� ������������� ��������� \n";
            cout << "3)���������� ��������� �� ����������� �������� ���������\n";
            cout << "4)������� ���������� �� ����������� �������� ���������\n";
            cout << "\n������� ����� ���������� ������: ";
            int sort;
            cin >> sort;
            switch (sort)
            {
            case 1:
                InsertionSort(filtered, new_size);
                break;
            case 2:
                Quick(filtered, new_size);
                break;
            case 3:
                InsertionSort_str(filtered, new_size);
                mess_sort(filtered, new_size);
                break;
            case 4:
                Quick_str(filtered, new_size);
                mess_sort(filtered, new_size);
                break;
            default:
                throw "������������ ����� ������";
            }
            for (int i = 0; i < new_size; i++)
            {
                output(filtered[i]);
            }
            delete[] filtered;
        }
        for (int i = 0; i < size; i++)
        {
            delete subscriptions[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
    return 0;
}





 






