
#ifndef PROTOKOL_SUBSCRIPTION_H
#define PROTOKOL_SUBSCRIPTION_H

#include "constants.h"


struct date
{
    int hour;
    int min;
    int second;
};

struct data1
{
    int bite;
};

struct protokol_subscription
{
    date start;
    date finish;
    data1 receive;
    data1 send;
    char mess[MAX_STRING_SIZE];
    int length;
    int sum;
};



#endif